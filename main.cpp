#include <iomanip>
#include <climits>
#include <iostream>
#include <iterator>
#include <map>
#include <typeinfo>
#include <vector>
#include <cmath>
#include <fstream>
#include <string>
#include <string.h>
#include <algorithm>
#include "Graph.h"
#include "Matrix.h"
using namespace std;
/*
const unsigned ADJACENCY_MATRIX[VERTICES][VERTICES] = {
    {0, 321, 733, 5, 469, 5},
    {242, 0, 5, 602, 5, 5},
    {467, 5, 0, 412, 5, 5},
    {5, 842, 356, 0, 5, 213},
    {5, 5, 5, 1107, 0, 895},
    {5, 5, 5, 5, 5, 0}};
 */
std::pair<int, std::string> simplePathfindingAlgoritm(Graph<int, bool, int>& graph, int key_t, int key_f)
/**
  /brief    переписываем граф в матрицу
 */
{
    Matrix<int> A(graph.size(), graph.size());
    for (auto& [key, node] : graph){
        try{
            if((node.size() + 1) != graph.size()){
                throw 1;
            }
        }
        catch(int v){
            cout<<"matrix not kvadrat";
            exit(1);
        }
        for (const auto& [key1, weight] : node){
            try{
                if (weight < 0)
                {
                    throw 1;
                }
                A.m_ptr[key][key1] = weight;
            }
            catch(int v){
                cout<<"- Weeeeight";
                exit(1);
            }
        }
    }
    /**
      /brief    делаем два массива посещенных вершин и суммарного короткого пути
     
        и создаем переменные с мин индексом и мин весом
     */
    graph.at(key_t);
    graph.at(key_f);
    unsigned long VERTICES = graph.size();
    bool visited[VERTICES];
    unsigned distances[VERTICES];
    unsigned minimalWeight, minimalIndex;
    
    for (unsigned i = 0; i < VERTICES; ++i)
    {
        visited[i] = false;
        distances[i] = INT_MAX;
    }
    /**
      /brief устанавливаем в массиве дистанций минимальное значение у ключа от которого начинаем идти
     */
    distances[key_t] = 0;
    /**
      /brief создаем переменные минмального веса и минимального индекса чтобы знать какую вершину рассматривать следущую и какой нее будет вес
     */
    do
    {
        minimalIndex = INT_MAX;
        minimalWeight = INT_MAX;
        
        for (unsigned i = 0; i < VERTICES; ++i)
        {
            if (!visited[i] && distances[i] < minimalWeight)
            {
                minimalIndex = i;
                minimalWeight = distances[i];
            }
        }
            
        if (minimalIndex != INT_MAX)
        {
            for (unsigned i = 0; i < VERTICES; ++i)
            {
                if (A.m_ptr[minimalIndex][i])
                {
                    unsigned temp = minimalWeight + A.m_ptr[minimalIndex][i];
                    
                    if (temp < distances[i])
                        distances[i] = temp;
                }
            }
        
            visited[minimalIndex] = true;
        }
    }
    while (minimalIndex < INT_MAX);
    int a;
    std::string s;
    //for (unsigned i = 0; i == VERTICES; ++i)
    if (distances[key_f] != INT_MAX)
    {
        a = distances[key_f];
        unsigned end = key_f;
        unsigned weight = distances[end];
        std::string way = std::to_string(end) + " - ";
        /**
          /brief  получаем путь из стартовой точки в конечныую
         */
        while (end != key_t)
        {
            for (unsigned j = 0; j < VERTICES; ++j)
            {
                if (A.m_ptr[j][end])
                {
                    int temp = weight - A.m_ptr[j][end];
                    
                    if (temp == distances[j])
                    {
                        end = j;
                        weight = temp;
                        way += std::to_string(j) + " - ";
                    }
                }
            }
        }
        return pair<int, std::string>(a, way);
    }
    else{
        cout << "Вес: " << key_t << " - " << key_f << " = " << "маршрут недоступен" << endl;
        exit(1);
    }
}
template<typename Graph>
void print(const Graph& graph) {
    if (graph.empty()) {
        std::cout << "> This graph is empty!" << std::endl;
        return; }
    std::cout << "> Size of graph: " << graph.size() << std::endl;
    for (const auto& [key, node] : graph) {
        std::cout << '[' << key << "] stores: " << node.value()<< " and matches with:" << std::endl;
        for (const auto& [key, weight] : node)
            std::cout << "\t[" << key << "]\t with weight: " << weight << std::endl;
    }
}
int main()
{
    /**
      /brief заполняем граф чтобы сделать его подобным матрице сверху
     */
    Graph<int, bool, int> graph;
    graph.insert_node(0, false);
    graph.insert_node(1, false);
    graph.insert_node(2, false);
    graph.insert_node(3, false);
    graph.insert_node(4, false);
    graph.insert_node(5, false);
    graph.insert_edge( 0, 1 , 321);
    graph.insert_edge( 0, 3 , 5);
    graph.insert_edge( 0, 5 , 5);
    graph.insert_edge( 0, 2 , 733);
    graph.insert_edge( 0, 4 , 469);
    graph.insert_edge( 1, 0 , 242);
    graph.insert_edge( 1, 2 , 5);
    graph.insert_edge( 1, 4 , 5);
    graph.insert_edge( 1, 5 , 5);
    graph.insert_edge( 1, 3 , 602);
    graph.insert_edge( 2, 0 , 467);
    graph.insert_edge( 2, 1 , 5);
    graph.insert_edge( 2, 4 , 5);
    graph.insert_edge( 2, 5 , 5);
    graph.insert_edge( 2, 3 , 412);
    graph.insert_edge( 3, 0 , 5);
    graph.insert_edge( 3, 4 , 5);
    graph.insert_edge( 3, 1 , 842);
    graph.insert_edge( 3, 2 , 356);
    graph.insert_edge( 3, 5 , 213);
    graph.insert_edge( 4, 3 , 1107);
    graph.insert_edge( 4, 5 , 895);
    graph.insert_edge( 4, 0 , 5);
    graph.insert_edge( 4, 1 , 5);
    graph.insert_edge( 4, 2 , 5);
    graph.insert_edge( 5, 0 , 5);
    graph.insert_edge( 5, 1 , 5);
    graph.insert_edge( 5, 2 , 5);
    graph.insert_edge( 5, 3 , 5);
    graph.insert_edge( 5, 4 , 5);
    auto[wes, put] = simplePathfindingAlgoritm(graph, 2, 3);
    cout<<"\n";
    cout<<"wes "<<wes<<"\n";
    cout << "Путь: ";
    for (int j = put.length()-3; j >= 0; --j)
        cout << put[j];
    cout<<"\n";
    /*
    Graph<std::string, Point, double> graph;
    graph["zero"]; // Заполнится точкой, которая заполнится нулями
    auto [it1, flag1] = graph.insert_node("first", {1, 1, 1}); std::cout << std::boolalpha << flag1 << std::endl; // => true
    graph["second"]; // Заполнится точкой, которая заполнится нулями
    auto [it2, flag2] = graph.insert_or_assign_node("second", {2, 2, 2}); // перезаполнит
    std::cout << std::boolalpha << flag2 << std::endl; // => false
    graph["third"] = Point{ 3, 3, 3 };
    auto [it3, flag3] = graph.insert_node("third", {1, 1, 1}); // бездействует std::cout << std::boolalpha << flag3 << std::endl; // => false
    graph["fourth"]; // Заполнится точкой, которая заполнится нулями graph.at("fourth") = Point{ 4, 4, 4 };
    auto [it4, flag4] = graph.insert_edge("first", "second" , 44.44); std::cout << std::boolalpha << flag4 << std::endl; // => true
    auto [it5, flag5] = graph.insert_edge("first", "second", 55.55); std::cout << std::boolalpha << flag5 << std::endl; // => false
    auto [it6, flag6] = graph.insert_or_assign_edge("first", "second", 66.66); std::cout << std::boolalpha << flag6 << std::endl; // => false
    auto [it7, flag7] = graph.insert_or_assign_edge("second", "first", 77.77); std::cout << std::boolalpha << flag7 << std::endl; // => true
    auto graph_other = graph; // Конструктор копирования
    auto graph_new = std::move(graph); // Конструктор перемещения
    graph = std::move(graph_new); // Перемещающее присваивание
    graph_new = graph; // Копирующее присваивание
    print(graph);
    graph.swap(graph_new); // Поменять местами содержимое графов
    swap(graph, graph_new); // Поменять местами содержимое графов
    print(graph);
     */
     
    return 0;
}
