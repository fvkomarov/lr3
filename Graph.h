//
//  Graph.h
//  dsdw
//
//  Created by Федор Комаров on 09.03.2023.
//
#include <iomanip>
#include <climits>
#include <iostream>
#include <iterator>
#include <map>
#include <typeinfo>
#include <vector>
#include <cmath>
#include <fstream>
#include <string>
#include <string.h>
#include <algorithm>

#ifndef Graph_h
#define Graph_h
struct Point { double x, y, z; };
std::ostream& operator << (std::ostream& out, Point p) {
std::cout << '(' << p.x << ',' << p.y << ',' << p.z << ')';
return out;
}
using namespace std;;

template <typename key_type, typename value_type, typename weight_type>
class Graph{
public:
    /**
      /brief делаем класс узла
     */
    class Node{
            public:
        /**;
              /brief значение узла и словарь ключей "в которые идет узел" с весами
             */
                value_type valu;
                map<key_type, weight_type> us_k_w;
            /**
              /brief дефолтный конструктор
             */
                Node(){
                    us_k_w = {};
                }
            /**
              /brief конструктор копирования
             */
                Node(const Node &x){
                    valu = x.valu;
                    us_k_w.insert(x.us_k_w.begin(), x.us_k_w.end());
                }
            /**
              /brief конструктор перемещения
             */
                Node(Node &&x){
                    valu = x.valu;
                    us_k_w.insert(x.us_k_w.begin(), x.us_k_w.end());
                    x.us_k_w.clear();
                }
            /**
              /brief оператор присваивания
             */
                Node& operator=(const Node& x)
                {
                    if(this != &x){
                        valu = x.valu;
                        us_k_w.insert(x.us_k_w.begin(), x.us_k_w.end());
                    }
                    return *this;
               }
            /**
              /brief оператор перемещения
             */
               Node& operator=(const Node&& x)
               {
                    if(this != &x){
                        valu = x.valu;
                        us_k_w.insert(x.us_k_w.begin(), x.us_k_w.end());
                        x.us_k_w.clear();
                    }
                    return *this;
               }
            /**
              /brief проеряет узел на пустоту соединений(изолированный)
             */
               bool empty(){
                   return (us_k_w.empty());
               }
            /**
              /brief выводит размер узла(вершин в которые входит узел)
             */
                unsigned size(){
                   return (us_k_w.size());
               }
            /**
              /brief выводит знчение
             */
               value_type& value(){
                   return(valu);
               }
               /*
                value_type& value(new_val){
                   valu = new_val
                   return(valu);
               }
               */
            /**
              /brief делает узел изолированным то есть изолированным
             */
               void clear(){
                   us_k_w.clear();
               }
            /**
              /brief итераторы по узлу
             */
               typename std::map<key_type, weight_type>::iterator begin(){
                 return (this->us_k_w.begin());
               }
               typename std::map<key_type, weight_type>::iterator end(){
                 return (this->us_k_w.end());
               }
               typename std::map<key_type, weight_type>::iterator begin() const {
                 return (this->us_k_w.begin());
               }
               typename std::map<key_type, weight_type>::iterator end() const {
                 return (this->us_k_w.end());
               }
               typename std::map<key_type, weight_type>::iterator cbegin() const{
                 return begin();
               }
               typename std::map<key_type, weight_type>::iterator cend() const {
                  return end();
               }
            /**
              /brief удаляем связь с другим узлом
             */
               bool erase_edge(key_type key){
                   if(us_k_w.count(key) == 0){
                       return false;
                   }
                   us_k_w.erase(key);
                   return true;
               }
               
        };
        map<key_type, Node> k_us;
    /**
      /brief дефолтный конструктор
     */
        Graph(){
            k_us = {};
        }
    /**
      /brief конструктор копирования
     */
        Graph (const Graph < key_type, value_type, weight_type > &x)
         {
            k_us.insert(x.k_us.begin(), x.k_us.end());
          }
    /**
      /brief конструктор перемещения
     */
         Graph (Graph < key_type, value_type, weight_type > &&x)
          {
            k_us.insert(x.k_us.begin(), x.k_us.end());
            x.k_us.clear();
          }
    /**
      /brief оператор присваивания
     */
          Graph& operator=(const Graph& x)
          {
            if(this != &x){
                k_us.insert(x.k_us.begin(), x.k_us.end());
            }
            return *this;
          }
    /**
      /brief оператор перемещения
     */
          Graph& operator=(Graph&& x)
          {
            if(this != &x){
                k_us.insert(x.k_us.begin(), x.k_us.end());
                x.k_us.clear();
            }
            return *this;
          }
    /**
      /brief проверяет на пустоту граф
     */
          bool empty(){
            return (k_us.empty());
          }
    /**
      /brief кол-во узлов в графе
     */
          unsigned long size(){
            return (k_us.size());
          }
    /**
      /brief делает пустой граф
     */
          void clear(){
            k_us.clear();
          }
    /**
      /brief меняет графы
     */
          void swap(Graph <key_type, value_type, weight_type> &x){
            Graph D(*this);
            *this = x;
            x = D;
          }
    /**
      /brief итераторы для графа
     */
          typename std::map<key_type, Node>::iterator begin(){
            return (k_us.begin());
          }
          typename std::map<key_type, Node>::iterator end(){
            return (this->k_us.end());
          }
          typename std::map<key_type, Node>::iterator begin() const {
            return (k_us.begin());
          }
          typename std::map<key_type, Node>::iterator end() const {
            return (this->k_us().end());
          }
          typename std::map<key_type, Node>::iterator cbegin() const{
              return begin();
          }
          typename std::map<key_type, Node>::iterator cend() const {
              return end();
          }
    /**
      /brief колличество ключей которые входят в данный
     */
          unsigned int degree_in(key_type key){
              unsigned int k = 0;
              for(auto it = k_us.begin(); it != k_us.end(); ++it){
                  if(it->first != key){
                      if(it->second.us_k_w.count(key) == 1){
                          k ++;
                      }
                  }
              }
              return(k);
          }
    /**
     /brief кол-во std::coutей которые выходят
     */
          unsigned int degree_out(key_type key){
              return (k_us[key].size());
          }
    /**
      /brief если есть путь  себя
     */
          bool loop(key_type key) {
              try{
                  if(k_us.count(key) == 0){
                      throw 1;
                  }
                  if(k_us[key].us_k_w.count(key) == 1){
                      return true;
                  }
                  else{
                      return false;
                  }
              }
              catch(int v){
                  cout<<"key not found"<<"\n";
                  exit(1);
              }
          }
    /**
      /brief опертор []
     */
            value_type& operator[](const key_type& key){
                auto it = k_us.find(key);
                if (it == k_us.end()){
                    it = k_us.insert({key, Node()}).first;
                }
                return it->second.value();
            }
    /**
      /brief вывод значения ключа
     */
          value_type& at(const key_type key){
              try{
                  if(k_us.count(key) == 1){
                    return (k_us[key].value());
                  }
                  else{
                      throw 1;
                  }
              }
              catch(int v){
                  cout<<"node not found"<<"\n";
                  exit(1);
              }
          }
    /**
      /brief добавление узла
     */
          std::pair<typename std::map<key_type, Node>::iterator, bool> insert_node(key_type key, value_type val){
              if(k_us.count(key) == 1){
                  return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), false);
              }
              Node a;
              a.valu = val;
              k_us[key] = a;
              return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), true);
          }
    /**
      /brief добавление или обновление узла
     */
          std::pair<typename std::map<key_type, Node>::iterator, bool> insert_or_assign_node(key_type key, value_type val){
              if(k_us.count(key) == 1){
                  k_us[key].valu = val;
                  return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), true);
              }
              Node a;
              a.valu = val;
              k_us[key] = a;
              return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), true);
          }
    /**
      /brief добавление ребра с весом
     */
          std::pair<typename std::map<key_type, Node>::iterator, bool> insert_edge(key_type key_from, key_type key_to, weight_type weight){
                try{
                    if(k_us.count(key_to) == 0 || k_us.count(key_from) == 0){
                        throw 1;
                    }
                    if(k_us[key_from].us_k_w.count(key_to) == 1){
                        return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), false);
                    }
                    k_us[key_from].us_k_w[key_to] = weight;
                    return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), true);
                }
                catch(int v){
                    cout<<"any key not found"<<"\n";
                    exit(1);
                }
          }
    /**
      /brief добавление или обновление веса у ребра
     */
          std::pair<typename std::map<key_type, Node>::iterator, bool> insert_or_assign_edge(key_type key_from, key_type key_to, weight_type weight){
                try{
                    if(k_us.count(key_to) == 0 || k_us.count(key_from) == 0){
                        throw 1;
                    }
                    k_us[key_from].us_k_w[key_to] = weight;
                    return pair<typename std::map<key_type, Node>::iterator, bool>(begin(), true);
                }
                catch(int v){
                    cout<<"any key not found"<<"\n";
                    exit(1);
                }
          }
    /**
      /brief все узлы становятся изолированными
     */
          void clear_edges(){
              for(auto it = k_us.begin(); it != k_us.end(); ++it){
                  it->second.clear();
              }
          }
    /**
      /brief удаляем ребра в которые идем из данного ключа
     */
          bool erase_edges_go_from(key_type key){
              if(k_us.count(key) == 0){
                  return false;
              }
              k_us[key].clear();
              return true;
          }
    /**
      /brief делаем узел изолированным
     */
          bool erase_edges_go_to(key_type key){
            int k = 0;
            for(auto it = k_us.begin(); it != k_us.end(); ++it){
                  if(it->first != key){
                      if(it->second.us_k_w.count(key) == 1){
                          it->second.us_k_w.erase(key);
                          k++;
                      }
                  }
              }
            if(k == 0){
                return false;
            }
            else{
                return true;
            }
          }
          bool erase_node(key_type key){
              if(k_us.count(key) == 0){
                  return false;
              }
              k_us.erase(key);
              return true;
          }
};
/**
  /brief функция вывода графа
 */
template<typename Graph>
void print(Graph& graph) {
    if (graph.empty()) {
        std::cout << "> This graph is empty!" << std::endl;
        return; }
    std::cout << "> Size of graph: " << graph.size() << std::endl;
    for (auto& [key, node] : graph) {
        std::cout << '[' << key << "] stores: " << node.value()<< " and matches with:" << std::endl;
        for (auto& [key, weight] : node){
            std::cout << "\t[" << key << "]\t with weight: " << weight << std::endl;
        }
    }
}
#endif /* Graph_h */
