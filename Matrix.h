//
//  Matrix.h
//  dsdw
//
//  Created by Федор Комаров on 09.03.2023.
//

#ifndef Matrix_h
#define Matrix_h
/**
  /brief класс матрицы функции из ЛР2  которые не нужны в ЛР 3  поэтому комментировать их я не буду
 */
template < typename T = double >class Matrix
{
public:
    /**
      /brief создаем размеры и двумерный массив
     */
    int m_columns;
    int m_rows;
    T **m_ptr;
    /**
      /brief дефолтный конструктор с пустой матрицей и 0 размерами
     */
    Matrix ()
  {
    m_rows = 0;
    m_columns = 0;
  }
    /**
      /brief  создаем пустую матрицу с заданными размерами
     */
    Matrix (int a, int b = 1)
    {
    m_rows = a;
    m_columns = b;
    m_ptr = new T* [m_rows];
    for (int i = 0; i < m_rows; i++)
      {
    m_ptr[i] = new T[m_columns];
      }
  }
    /**
      /brief конструктор копирования
     */
  Matrix (const Matrix < T > &x)
  {
    m_columns = x.m_columns;
    cout<<m_columns<<'\n';
    m_rows = x.m_rows;
    cout<<m_rows<<'\n';
    m_ptr = new T* [m_rows];
    for (int i = 0; i < m_rows; i++)
      {
        m_ptr[i] = new T[m_columns];
        std::copy(x.m_ptr[i], x.m_ptr[i] + m_columns, m_ptr[i]);// 2 цикл
      }
  }
    /**
      /brief конструктор перемещения
     */
  Matrix (Matrix < T > &&x)
  {
    m_columns = x.m_columns;
    m_rows = x.m_rows;
    m_ptr = x.m_ptr ;
    x.m_columns = 0;
    x.m_rows = 0;
    x.m_ptr = NULL;
  }
    /**
      /brief деструктор для удаления динамической памяти
     */
  ~Matrix()
  {
    for (int i = 0; i < m_rows; i++)
      {
    delete[]m_ptr[i];
      }
    delete[]m_ptr;
  }
    /**
      /brief конструктор инициализатор матричый
     */
Matrix (std::initializer_list < std::initializer_list<T> > lst):
  Matrix (lst.size ())
  {
    int count = 0;
    int k = 0;
  for (auto element:lst)
      {
    k = element.size();
    delete[]m_ptr[count];
    m_ptr[count] = new T[k];
    int i = 0;
      for (auto el:element)
      {
        m_ptr[count][i] = el;
        cout<<m_ptr[count][i]<<'\t';
        ++i;
      }
      cout<<'\n';
    ++count;
      }
      m_columns = k;
  }
    /**
      /brief конструктор инициализатор для вектора
     */
Matrix (std::initializer_list < T > lst):
  Matrix (static_cast < T > (lst.size ()))
  {
  int count = 0;
  for (auto element:lst)
      {
      m_ptr[count][0] = element;
      cout<<m_ptr[count][0]<<'\t';
      cout<<'\n';
      ++count;
      }
  }
    /**
      /brief опертор сложения матриц
     */
  Matrix  operator+ (const Matrix < T > &D)
  {
    try{
        if (m_columns != D.m_columns || m_rows != D.m_rows){
            throw 1;
        }
        Matrix<T> A(m_rows, m_columns);
        for (int i = 0; i < m_rows; i++){
            for (int j = 0; j < m_columns; j++){
                A.m_ptr[i][j] = m_ptr[i][j] + D.m_ptr[i][j];
            }
        }
        return A;
    }
    catch(int v){
        cout<<"сложение невозможно"<<"\n";
        exit(1);
    }
  }
    /**
      /brief оператор вычитания матриц
     */
  Matrix  operator- (const Matrix < T > &D)
  {
    try{
        if (m_columns != D.m_columns || m_rows != D.m_rows){
            throw 1;
        }
        Matrix<T> A(m_rows, m_columns);
        for (int i = 0; i < m_rows; i++){
            for (int j = 0; j < m_columns; j++){
                A.m_ptr[i][j] = m_ptr[i][j] - D.m_ptr[i][j];
            }
        }
        return A;
    }
    catch(int v){
        cout<<"сложение невозможно"<<"\n";
        exit(1);
    }
  }
    /**
      /brief оператор умножения матриц на матрицу
     */
  Matrix operator *(const Matrix < T > &D){
        try{
            if (m_columns != D.m_rows){
                throw 1;
            }
            Matrix<T> A(m_rows, D.m_columns);
            for(int i=0; i < m_rows; i++){
                for(int j=0; j < D.m_columns; j++){
                    for(int k=0; k < m_columns; k++){
                        A.m_ptr[i][j]+=m_ptr[i][k]*D.m_ptr[k][j];
                    }
                }
            }
            return A;
        }
        catch(int v){
        cout<<"умножение невозможно"<<"\n";
        exit(1);
    }
  }
    /**
      /brief оператор умножения матриц на число
     */
  Matrix operator *(const double a){
    Matrix<T> A(m_rows, m_columns);
    for (int i = 0; i < m_rows; i++){
        for (int j = 0; j < m_columns; j++){
            A.m_ptr[i][j] = m_ptr[i][j]*a;
        }
    }
    return A;
  }
    /**
      /brief += для матриц
     */
  void operator += (const Matrix < T > &D)
  {
    try{
        if (m_columns != D.m_columns || m_rows != D.m_rows){
            throw 1;
        }
        for (int i = 0; i < m_rows; i++){
            for (int j = 0; j < m_columns; j++){
                m_ptr[i][j] += D.m_ptr[i][j];
            }
        }
    }
    catch(int v){
        cout<<"сложение невозможно"<<"\n";
        exit(1);
    }
  }
    /**
      /brief оператор -= для матриц
     */
  void operator-= (const Matrix < T > &D)
  {
    try{
        if (m_columns != D.m_columns || m_rows != D.m_rows){
            throw 1;
        }
        for (int i = 0; i < m_rows; i++){
            for (int j = 0; j < m_columns; j++){
                m_ptr[i][j] -= D.m_ptr[i][j];
            }
        }
    }
    catch(int v){
        cout<<"вычитание невозможно"<<"\n";
        exit(1);
    }
  }
    /**
      /brief опертор *= на число для матриц
     */
  Matrix operator *= (const double a){
    Matrix<T> A(m_rows, m_columns);
    for (int i = 0; i < m_rows; i++){
        for (int j = 0; j < m_columns; j++){
            m_ptr[i][j] =m_ptr[i][j] * a;
        }
    }
    return A;
  }
    /**
      /brief опертор присваивания для матриц
     */
  Matrix& operator=(const Matrix& x)
   {
      if (this != &x)
      {
        for (int i = 0; i < m_rows; i++)
        {
            delete[]m_ptr[i];
         }
        delete[]m_ptr;
        m_columns = x.m_columns;
        m_rows = x.m_rows;
        m_ptr = new T* [m_rows];
        for (int j = 0; j < m_rows; j++)
          {
            m_ptr[j] = new T[m_columns];
            std::copy(x.m_ptr[j], x.m_ptr[j] + m_columns, m_ptr[j]);
          }
        }
        return *this;
   }
    /**
      /brief опертор переемешения для матриц
     */
    Matrix& operator=(Matrix&& D)
    {
       if (this != &D)
       {
          for (int i = 0; i < m_rows; i++)
          {
            delete[]m_ptr[i];
          }
          delete[]m_ptr;
          m_ptr = D.m_ptr;
          m_columns = D.m_columns;
          m_rows = D.m_rows;
          D.m_ptr = NULL;
          D.m_columns = 0;
          D.m_rows = 0;
       }
       return *this;
    }
    void GetMatr(T **mas, T **p, int i, int j, int m) {
      int ki, kj, di, dj;
      di = 0;
      for (ki = 0; ki<m - 1; ki++) {
        if (ki == i) di = 1;
        dj = 0;
        for (kj = 0; kj<m - 1; kj++) {
          if (kj == j) dj = 1;
          p[ki][kj] = mas[ki + di][kj + dj];
        }
      }
    }
    int Determinant(T **mas, int m) {
      int i, j, d, k, n;
      T **p;
      p = new T*[m];
      for (i = 0; i<m; i++)
        p[i] = new T[m];
      j = 0; d = 0;
      k = 1;
      n = m - 1;
      if (m<1) cout << "Определитель вычислить невозможно!";
      if (m == 1) {
        d = mas[0][0];
        return(d);
      }
      if (m == 2) {
        d = mas[0][0] * mas[1][1] - (mas[1][0] * mas[0][1]);
        return(d);
      }
      if (m>2) {
        for (i = 0; i<m; i++) {
          GetMatr(mas, p, i, 0, m);
          cout << mas[i][j] << endl;
          d = d + k * mas[i][0] * Determinant(p, n);
          k = -k;
        }
      }
      return(d);
    }
    int det(){
        try{
          if(m_rows != m_columns){
              throw 1;
          }
          return Determinant(m_ptr, m_rows);
        }
        catch(int v){
           cout<<"вычислить определитель невозможно"<<'\n';
           exit(1);
        }
    }
    int Rang(int n,int m, T **a){
        const double EPS = 1E-9;
        int rank = max(n,m);
        vector<char> line_used (n);
        for (int i=0; i<m; ++i) {
        int j;
        for (j=0; j<n; ++j)
        
        if (!line_used[j] && abs(a[j][i]) > EPS) break;
        
        if (j == n) --rank;
        
        else {
        
        line_used[j] = true;
        
        for (int p=i+1; p<m; ++p){
            a[j][p] /= a[j][i];
            for (int k=0; k<n; ++k){
                if (k != j && abs (a[k][i]) > EPS){
                    for (int p=i+1; p<m; ++p){
                        a[k][p] -= a[j][p] * a[k][i];
                    }
                }
            }
        }
        }
        }
        return rank;
    }
    int rank(){
        return Rang(m_rows, m_columns, m_ptr);
    }
    double norm(){
        double ans = 0;
        for(int i = 0; i < m_rows; i++){
            for(int j = 0;j < m_columns; j++){
                ans += pow(m_ptr[i][j], 2);
            }
        }
        return sqrt(ans);
    }
};
template <typename T = double>
void pm(const Matrix<T> &D){
    for (int i = 0; i<D.m_rows; i++){
    for (int j = 0;j<D.m_columns;j++){
        cout<<D.m_ptr[i][j]<<'\t';
    }
    cout<<'\n';
    }
}

template <typename T = double>
Matrix<T> transpose(const Matrix<T>& D){
        Matrix<T> A(D.m_columns, D.m_rows);
        for(int i = 0;i<D.m_rows;i++){
            for(int j = 0;j<D.m_columns;j++){
                A.m_ptr[j][i] = D.m_ptr[i][j];
            }
        }
        return A;
    }
template <typename T = double>
Matrix < double >
inv (Matrix<T> & D)
{
  try
  {
    double rd = D.det ();
    cout << rd << '\n';
    if ((D.m_rows != D.m_columns) || (rd == 0))
      {
    throw 1;
      }
    Matrix < double >A (D.m_rows, D.m_columns);
    T d;
    for (int i = 0; i < D.m_rows; i++)
      {
    for (int j = 0; j < D.m_columns; j++)
      {
        Matrix < T >p (D.m_rows - 1, D.m_rows - 1);
        p.GetMatr (D.m_ptr, p.m_ptr, i, j, D.m_rows);
        pm (p);
        d = p.det ();
        if ((i + j) % 2 == 0)
          {
        A.m_ptr[i][j] = d;
          }
        else
          {
        A.m_ptr[i][j] = -d;
          }
      }
      }
    double f = (double) 1 / rd;
    A = transpose (A);
    pm (A);
    cout << '\n';
    for (int i = 0; i < A.m_rows; i++)
      {
    for (int j = 0; j < A.m_columns; j++)
      {
        A.m_ptr[i][j] = A.m_ptr[i][j] * f;
      }
      }
    pm (A);
    return A;
  }
  catch (int v)
  {
    cout << "нет обратной";
    exit(1);
  }
}

template <typename T = double>
Matrix<T> pow(const Matrix<T>& D, int a){
    Matrix<T> A(D);
    for(int i = 0;i<a - 1;i++){
        A = A * D;
    }
    return A;
}


#endif /* Matrix_h */
